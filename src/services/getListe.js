
import param from '@/param/param'

export default {
    getListe() {
        return axios.get(param.listeFilms)
            .then(response => {
                return response.data
            })
    },
    getListe2() {
        return axios.get(param.listeBio)
            .then(response => {
                return response.data
            })
    },
    getListe3() {
        return axios.get(param.listeNomi)
            .then(response => {
                return response.data
            })
    },
    getListe4() {
        return axios.get(param.listeFiche)
            .then(response => {
                return response.data
            })
    },
}