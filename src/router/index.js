import Vue from 'vue'
import Router from 'vue-router'

import Presentation from '@/components/Presentation'
import Biographie from '@/components/Biographie'
import Filmographie from '@/components/Filmographie'
import Nominations from '@/components/Nominations'
import Fiche from '@/components/Fiche'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'Presentation', component: Presentation },
    { path: '/biographie', name: 'Biographie', component: Biographie },
    { path: '/filmographie', name: 'Filmographie', component: Filmographie },
    { path: '/nominations', name: 'Nominations', component: Nominations },
    { path: '/fiche', name: 'Fiche', component: Fiche },
  ]
})
